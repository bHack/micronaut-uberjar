package com.testapp

import io.micronaut.core.util.CollectionUtils
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.views.View

@Controller("/")
class ViewsController {

    @View("main")
    @Get("/")
    fun index(): HttpResponse<Any> {
        return HttpResponse.ok(CollectionUtils.mapOf("test", "test"))
    }
}
